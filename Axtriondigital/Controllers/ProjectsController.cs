﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axtriondigital.ViewModels;
using BusinessServices.Interfaces;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace Axtriondigital.Controllers
{
    public class ProjectsController : Controller
    {
        private IProjectService _projects;
        public ProjectsController(IProjectService projects)
        {
            _projects = projects;
        }

        [Route("projects/projectlist")]
        public IActionResult ProjectsList(string sortOrder , string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            var projectModels = _projects.GetAllProjects();

            if (!String.IsNullOrEmpty(searchString))
            {
                projectModels = projectModels.Where(s => s.Name.Contains(searchString));
            }

            var listingResult = projectModels
                .Select(result => new ProjectViewModel
                {
                    ProjectId = result.Id,
                    ProjectName = result.Name
                });

            switch (sortOrder)
            {
                case "name_desc":
                    listingResult = listingResult.OrderByDescending(s => s.ProjectName);
                    break;              
                default:
                    listingResult = listingResult.OrderBy(s => s.ProjectName);
                    break;
            }

            return View(listingResult);
        }

        [HttpPost]      
        public ActionResult ProjectDetails(string projectId)
        {
            return PartialView("_ProjectDetailPopup", _projects.GetProjectById(Convert.ToInt32(projectId)));
        }
    }
}