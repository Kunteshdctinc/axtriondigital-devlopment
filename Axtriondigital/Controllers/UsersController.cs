﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axtriondigital.ViewModels;
using BusinessServices.Interfaces;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace Axtriondigital.Controllers
{
    public class UsersController : Controller
    {
        private IUserService _users;
        private readonly IProjectService _projectService;

        public UsersController(IUserService usersservice,IProjectService projectService)
        {
            this._users = usersservice;
            this._projectService = projectService;
        }

        [Route("users/userlist")]
        public IActionResult UsersList()
        {
            ViewBag.Projects = _projectService.GetAllProjects();

            var users = _users.GetAllUsers();
            return View(users);
        }

        [Route("users/adduser")]
        public IActionResult AddUser(UserAddModel user)
        {
            _users.AddUser(user);
            return RedirectToAction("Users");
        }
    }
}