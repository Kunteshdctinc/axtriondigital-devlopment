﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Axtriondigital.ViewModels
{
    public class UserProjectModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }       
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string PositionInProject { get; set; }
    }
}
