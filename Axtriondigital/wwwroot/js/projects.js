function showProject(projectid) {   
    $.ajax({
        type: "POST",
        url: "/Projects/ProjectDetails?projectId=" + projectid,      
        dataType: "html",
        success: function (response) {
            $('#dialog').html(response);

            var x = document.getElementById("popup");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }

            event.preventDefault();
           
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

}
function hideUser() {
        var x = document.getElementById("popup");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
