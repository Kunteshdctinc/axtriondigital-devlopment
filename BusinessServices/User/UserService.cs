﻿using BusinessServices.Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessServices
{
    public class UserService : IUserService
    {
        private AxtrionContext _context;

        public UserService(AxtrionContext context)
        {
            _context = context;
        }

        public void AddUser(UserAddModel user)
        {
            var newUser = new User
            {
                UserName = user.UserName,
                ProjectId = user.ProjectId,
                Position = user.Position
            };
            _context.Add(newUser);
            _context.SaveChanges();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _context.Users
                           .Include(user => user.Project);
                         
        }
    }
}
