﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessServices.Interfaces
{
    public interface IUserService
    {
        IEnumerable<User> GetAllUsers();
        void AddUser(UserAddModel newUser);
    }
}
