﻿using BusinessServices.Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessServices
{
    public class ProjectService : IProjectService
    {
        private AxtrionContext _context;

        public ProjectService(AxtrionContext context)
        {
            _context = context;
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _context.Projects
                           .Include(project => project.Users);

        }

        public Project GetProjectById(int projectid)
        {
            var projects = _context.Projects
                           .Include(project => project.Users)
                           .Include(project => project.ClientInfo)
                           .Include(project => project.Contacts);

            return projects.FirstOrDefault(p => p.Id == projectid);
        }

    }
}
