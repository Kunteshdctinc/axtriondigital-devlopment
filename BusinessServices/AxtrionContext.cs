﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace BusinessServices
{
    public class AxtrionContext : DbContext
    {
        public AxtrionContext(DbContextOptions<AxtrionContext> options) : base(options) { }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }        
    }
}
