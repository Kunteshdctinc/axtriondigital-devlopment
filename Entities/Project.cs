﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public ICollection<Contact> Contacts { get; set; }
        public string Address { get; set; }
        public string TeamLeader { get; set; }
        public string ProjectManager { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<User> Users { get; set; }
    }

    public class ClientInfo
    {   
        public int Id { get; set; }
        public string Name { get; set; }
        public string Orgranization { get; set; }
    }

    public class Contact
    {
        public int Id { get; set; }
        public string ContactName { get; set; }
    }
}
