﻿namespace Entities
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserPicture { get; set; }
        public string Contract { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public Position Position { get; set; }
    }

    public class UserAddModel
    {
        public string UserName { get; set; }
        public Position Position { get; set; }
        public int ProjectId { get; set; }
    }

    public enum Position
    {
        SoftwareDeveloper,
        SeniorSoftwareDeveloper,
        TeamLeader,
        BusinessAnalyst,
        ProjectManager,
        SolutionArchitect
    }


}
